package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/pennatus/pennatus/internal/pennatus"
)

func main() {
	app := &cli.App{
		Name:  "pennatus",
		Usage: "A command-line utility to guide you to create infrastructure assemblies",
		Commands: []*cli.Command{
			{
				Name:      "run",
				Usage:     "pennatus run [directory]",
				UsageText: "pennatus run ./infrastructureDirectory",
				Action: func(c *cli.Context) error {
					cc, err := pennatus.New(c.Args().First())
					if err != nil {
						return err
					}
					return cc.Run()
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
