package pennatus

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type Pennatus struct {
	rootDir  string
}

// New creates a Pennatus wizard
func New(rootDir string) (*Pennatus, error) {
	return &Pennatus{
		rootDir:  rootDir,
	}, nil
}

type DirectoryCheckResult struct {
	IsDirectory bool
	IsPennatusProject bool
}

func IsDirectory(rootDir string) bool {
	if stat, err := os.Stat(rootDir); err == nil && stat.IsDir() {
			return true
	}

	return false
}

func IsFile(rootDir string) bool {
	if stat, err := os.Stat(rootDir); err == nil && !stat.IsDir() {
			return true
	}

	return false
}

func IsPennatusProject(rootDir string) DirectoryCheckResult {
	if (IsDirectory(rootDir)) {
		yamlPath := filepath.Join(rootDir, "pennatus.yaml")
		if (IsFile(yamlPath)) {
			return DirectoryCheckResult{IsDirectory: true, IsPennatusProject: true}
		}

		return DirectoryCheckResult{IsDirectory: true, IsPennatusProject: false}
	}

	return DirectoryCheckResult{IsDirectory: false, IsPennatusProject: false}
}

type ProjectState struct {
	DirectoryFinalized bool
	RootDirectory string
	IsCreated bool
}

func GetProjectStateExplicitRootDir(rootDir string) ProjectState {
	explicitRootDirCheckResult := IsPennatusProject(rootDir)
	return ProjectState{RootDirectory: rootDir, IsCreated: explicitRootDirCheckResult.IsPennatusProject, DirectoryFinalized: true}
}

func GetProjectStateUnspecifiedRootDir() ProjectState {
	currentDirCheckResult := IsPennatusProject(".")
	if (currentDirCheckResult.IsPennatusProject) {
		return ProjectState{RootDirectory: ".", IsCreated: true, DirectoryFinalized: true}
	}

	return ProjectState{RootDirectory: "", IsCreated: false, DirectoryFinalized: false}
}

func GetProjectState(rootDirInput string) ProjectState {
	isExplicitRootDir := len(rootDirInput) > 0

  if (isExplicitRootDir) {
		return GetProjectStateExplicitRootDir(rootDirInput)
	} else {
		return GetProjectStateUnspecifiedRootDir()
	}
}

var promptMessage = "Provide the following information to create a Pennatus project."

func promptWithDefault(description string, prompt string, defaultValue string) string {
	fmt.Println(description)
	fmt.Printf("%s [%s]: ", prompt, defaultValue)
	scanner := bufio.NewScanner(os.Stdin)
	line := ""
	if scanner.Scan() {
		line = scanner.Text()
	}
	if (len(line) == 0) {
		return defaultValue
	}

	return line
}

func makeTerraformUsableName(name string) string {
	noSpaces := strings.ReplaceAll(
		strings.Trim(strings.ToLower(name), " \t"),
		" ",
		"_",
	)

	noDashes := strings.ReplaceAll(noSpaces, "-", "_")

	return noDashes
}

type ProjectConfig struct {
	AssemblyName string
	AssemblyDomainName string
	VaultDomainName string
	AssemblyProjectName string
	AssemblyInfrastructureWorkspaceName string
	GitlabProjectSlug string
}

func promptProjectInitialConfig(rootDir string) ProjectConfig {
	fmt.Println(promptMessage)
	defaultName := strings.Title(filepath.Base(rootDir))
	assemblyName := promptWithDefault(
		"The assembly name is the name that ties together all the base resources in your project.",
		"Assembly name",
		defaultName,
	)

	terraformUsableName := makeTerraformUsableName(assemblyName)

	assemblyInfrastructureWorkspaceName := promptWithDefault(
		"The assembly infrastructure workspace name is the name that Terraform will use to identify the assembly.",
		"Assembly infrastructure workspace name",
		terraformUsableName,
	)

	domainableOrgName := strings.ToLower(strings.ReplaceAll(assemblyName, " ", "-"))

	assemblyDomainName := promptWithDefault(
		"The assembly domain name is the URL that the project’s core resources will be accessed from over the internet.",
		"Assembly domain name",
		fmt.Sprintf("%s.com", domainableOrgName),
	)

	vaultDomainName := promptWithDefault(
		"The vault domain name is the URL of your project’s Vault instance.",
		"Vault domain name",
		fmt.Sprintf("vault.%s", assemblyDomainName),
	)

	assemblyProjectName := promptWithDefault(
		"The assembly project name is the name of the core infrastructure project for this assembly.",
		"Assembly project name",
		"is-assembly",
	)

	gitlabProjectSlug := promptWithDefault(
		"The GitLab project slug is the name of the GitLab project that will be created for this assembly.",
		"GitLab project slug",
		assemblyInfrastructureWorkspaceName,
	)

	return ProjectConfig{
		AssemblyName: assemblyName,
		AssemblyDomainName: assemblyDomainName,
		VaultDomainName: vaultDomainName,
		AssemblyProjectName: assemblyProjectName,
		AssemblyInfrastructureWorkspaceName: assemblyInfrastructureWorkspaceName,
		GitlabProjectSlug: gitlabProjectSlug,
	}
}

var gcpCookiecutterSource = "https://gitlab.com/pennatus/gcp-cookiecutter.git"

func createPennatusProject(rootDir string, projectInitialConfig ProjectConfig) error {
	orgNameWord := "org_name" + "=" + projectInitialConfig.AssemblyName
	orgDomainNameWord := "org_domain_name" + "=" + projectInitialConfig.AssemblyDomainName
	vaultDomainNameWord := "vault_domain_name" + "=" + projectInitialConfig.VaultDomainName
	orgProjectNameWord := "org_project_name" + "=" + projectInitialConfig.AssemblyProjectName
	orgInfrastructureWorkspaceNameWord := "org_infrastructure_workspace_name" + "=" + projectInitialConfig.AssemblyInfrastructureWorkspaceName
	gitlabProjectSlugWord := "gitlab_project_slug" + "=" + projectInitialConfig.GitlabProjectSlug

	stdout, err := exec.Command(
		"pipx",
		"run",
		"cookiecutter",
		gcpCookiecutterSource,
		"--output-dir",
		filepath.Dir(rootDir),
		"--no-input",
		orgNameWord,
		orgDomainNameWord,
		vaultDomainNameWord,
		orgProjectNameWord,
		orgInfrastructureWorkspaceNameWord,
		gitlabProjectSlugWord,
	).CombinedOutput()

	if err != nil {
		fmt.Println(string(stdout))
		return err
	}

	return nil
}

var welcomeMessage = "Pennatus is running"
var workingDirectoryPrompt = "Provide a directory to create a Pennatus project, or an existing directory to continue with setup:"
var directoryFailedMessage = "No valid directory provided"
var creatingPennatusProjectMessage = "Creating Pennatus project..."

func (cc *Pennatus) Run() error {
	fmt.Println(welcomeMessage)
	projectState := GetProjectState(cc.rootDir)

	if (!projectState.DirectoryFinalized) {
		fmt.Println(workingDirectoryPrompt)
		var promptedDir string
    fmt.Scanln(&promptedDir)
		projectState = GetProjectState(promptedDir)
	}

	if (!projectState.DirectoryFinalized) {
		fmt.Println(directoryFailedMessage)
		return nil
	}

	if (!projectState.IsCreated) {
		projectInitialConfig := promptProjectInitialConfig(projectState.RootDirectory)
		// TODO Ideally the explicit folder name should override the GitLab project slug; however, the cookiecutter
		// enforces the use of the GitLab project slug as the folder name. Until the cookiecutter template is modified
		// to allow for a different folder name, we have to update the RootDirectory to match this behaviour.
		projectState.RootDirectory = filepath.Join(filepath.Dir(projectState.RootDirectory), projectInitialConfig.GitlabProjectSlug)

		fmt.Println(creatingPennatusProjectMessage)
		err := createPennatusProject(projectState.RootDirectory, projectInitialConfig)

		if err != nil {
			fmt.Println(fmt.Sprintf("Error creating Pennatus project: %s", err))
			return nil
		}

		projectState.IsCreated = true
		fmt.Println(fmt.Sprintf("Pennatus project created at %s.", projectState.RootDirectory))
		fmt.Println(fmt.Sprintf("To continue setup, follow the instructions at %s/README.md", projectState.RootDirectory))
	}

	return nil
}
