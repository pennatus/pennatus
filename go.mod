module gitlab.com/pennatus/pennatus

go 1.14

require (
	github.com/arithran/go-cookiecutter v1.0.0 // indirect
	github.com/urfave/cli/v2 v2.2.0
)
