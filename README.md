
# pennatus-cli

Pennatus is a command line tool for setting up Pennatus infrastructure groups (orgs).

## Prerequisites

- Pipx
- Cookiecutter

## Demo

```sh
rm -rf sandbox; go run main.go run sandbox/my_project
```